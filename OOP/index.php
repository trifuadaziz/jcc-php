<?php
    require_once ('Animal.php');         // import class animal dari Animal.php di dalam index.php
    require_once ('Ape.php');            // import class Ape dari Ape.php di dalam index.php
    require_once ('Frog.php');           // import class Frog dari Frog.php di dalam index.php

    $sheep = new Animal("shaun");   // membuat object "sheep" dar class "Animal"

    echo "Name : " . $sheep -> name;            // Tampilkan nama dari Animal
    echo "<br>";
    echo "legs : " . $sheep -> legs;            // Tampilkan legs dari Animal
    echo "<br>";
    echo "cold blooded : " . $sheep -> cold_blooded;    // Tampilkan cold_blooded dari Animal
    echo "<br> <br>";


    $kodok = new Frog("buduk");                 // membuat object "kodok" dar class "Frog"

    echo "Name : " . $kodok -> name;            // Tampilkan nama dari kodok
    echo "<br>";
    echo "legs : " . $kodok -> legs;            // Tampilkan legs dari kodok
    echo "<br>";
    echo "cold blooded : " . $kodok -> cold_blooded;    // Tampilkan cold_blooded dari kodok
    echo "<br>";
    echo "Jump : " . $kodok -> jump();           // Tampilkan nilai fungsi dari jump() 
    echo "<br> <br>";


    $sunggokong = new Ape("kera sakti");        // membuat object "sunggokong" dar class "Ape"

    echo "Name : " . $sunggokong -> name;       // Tampilkan nama dari kodok
    echo "<br>";
    echo "legs : " . $sunggokong -> legs;       // Tampilkan legs dari kodok
    echo "<br>";
    echo "cold blooded : " . $sunggokong -> cold_blooded;    // Tampilkan cold_blooded dari kodok
    echo "<br>";
    echo "Yell : " . $sunggokong -> yell();     // Tampilkan nilai fungsi dari yell()
    echo "<br> <br>";
?>