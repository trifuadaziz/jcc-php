<?php
    require_once ('Animal.php');     //karena Ape.php turunan class Animal.php

    class Ape extends Animal{   //class Ape yg merupakan turunan class Animal
        public $legs = 2;

        public function yell(){
            return "Auooo";
        }
    }

?>